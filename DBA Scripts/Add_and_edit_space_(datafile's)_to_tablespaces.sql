/* Increasing the table space */ 
ALTER TABLESPACE DWH_MIS
ADD DATAFILE '<PATH_TO_TABLESPACE>'
SIZE 1000M; 

/* Resizing a table space file */ 
ALTER DATABASE DATAFILE '<PATH_TO_TABLESPACE>';
RESIZE 15000M;

/* AUTOEXTEND - automatic expansion of data file sizes in a table space to a specified maximum */ 
ALTER TABLESPACE DWH_MIS
ADD DATAFILE '<PATH_TO_TABLESPACE>' SIZE 1000M
AUTOEXTEND ON
NEXT 1000M
MAXSIZE 15000M;

---------------------------------------------------------------------
---------------------------------------------------------------------
ALTER DATABASE DATAFILE '<PATH_TO_TABLESPACE>'
   AUTOEXTEND ON
      NEXT 102400K;
      MAXSIZE 32000M;

---------------------------------------------------------------------
---------------------------------------------------------------------