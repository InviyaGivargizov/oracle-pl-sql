DECLARE
  
  CURSOR warn_free_space IS SELECT 	a.TABLESPACE_NAME tablespace_name,b.bytes total_mb, a.bytes free_mb,
                                    round(a.BYTES*100/b.BYTES,2) percent_free, round((b.BYTES-a.BYTES)*100/b.BYTES,2) percent_used
                            FROM  (select TABLESPACE_NAME, ROUND(SUM(bytes)/1024/1024) BYTES from dba_free_space group by TABLESPACE_NAME) a,
                                  (select TABLESPACE_NAME, ROUND(SUM(bytes)/1024/1024) BYTES from dba_data_files group by TABLESPACE_NAME) b
                            WHERE a.TABLESPACE_NAME=b.TABLESPACE_NAME
                            ORDER BY a.TABLESPACE_NAME;

  rwfs warn_free_space%ROWTYPE;
  
  
  vExpMsg nvarchar2(200);
                 
BEGIN                       
  OPEN warn_free_space;

       LOOP
        IF (rwfs.total_mb >= 10000) THEN
           IF (rwfs.free_mb <= 1000) THEN
              vExpMsg := 'Tablespace: '||rwfs.tablespace_name||' '||'| have low free space '||to_char(rwfs.free_mb)||' Mb';
              tas_util.send_telegrams('MNG_REGLAMENT_ERROR', '{!}ATTENTION{!}', vExpMsg );
              END IF;
              END IF;  
              FETCH warn_free_space INTO rwfs;
              EXIT WHEN warn_free_space%NOTFOUND;  
        END LOOP;
         
  CLOSE warn_free_space;
END;

