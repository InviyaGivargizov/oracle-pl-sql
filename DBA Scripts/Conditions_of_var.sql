---- Group that checks the contents of a string for a variety of conditions
-------------------------------------------------------

--Defined as a character that has an upper and lower representation
FUNCTION is_letter (character_in IN VARCHAR2)
      RETURN BOOLEAN
   IS 
      -- Test single character only!
      l_letter   VARCHAR2 (1);
   BEGIN
      l_letter := character_in;
      RETURN UPPER (character_in) <> LOWER (character_in);
   EXCEPTION
      WHEN VALUE_ERROR
      THEN
         DBMS_OUTPUT.put_line ('IS_LETTER must be passed a single character!');
         RAISE;
   END is_letter;
   
   
--Is the single character a digit (0 through 9)?   
FUNCTION is_digit (character_in IN VARCHAR2)
      RETURN BOOLEAN
   IS
      -- Test single character only!
      l_letter   CHAR (1);
   BEGIN
      l_letter := character_in;
      RETURN INSTR ('012345679', l_letter) > 0;
   EXCEPTION
      WHEN VALUE_ERROR
      THEN
         DBMS_OUTPUT.put_line ('IS_DIGIT must be passed a single character!');
         RAISE;
   END is_digit;
   
   
FUNCTION is_number (str_in IN VARCHAR2, 
                   numeric_format_in IN VARCHAR2 DEFAULT NULL)
      RETURN BOOLEAN
   IS
      dummy NUMBER;
   BEGIN
      IF str_in IS NULL
      THEN
         RETURN NULL;
      END IF;

      IF numeric_format_in IS NULL
      THEN
         dummy := TO_NUMBER (str_in);
      ELSE
         dummy := TO_NUMBER (str_in, numeric_format_in);
      END IF;

      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN FALSE;
   END is_number;
