----------------------------
/*----------------------------
Create a role 
----------------------------*/
CREATE ROLE <NAME_ROLE>;
----------------------------

/*----------------------------
Select for users role
----------------------------*/
select * from user_role_privs;
----------------------------

/*----------------------------
Loop for grant roles to  <NAME_ROLE> 
----------------------------*/
begin
for c in (select table_name, owner from dba_tables where owner = '<USER_NAME>')
  loop
    begin
      execute immediate
              'GRANT SELECT ON '||c.owner||'.' || c.table_name || ' to <NAME_ROLE> ';
    end;
  end loop;
end;
----------------------------

/*----------------------------
Give role to  <USER_NAME>
----------------------------*/
grant <NAME_ROLE>; to <USER_NAME>;
grant <NAME_ROLE>; to <USER_NAME>;
grant <NAME_ROLE>; to <USER_NAME>;
----------------------------

/*----------------------------
Revoke roles from  <USER_NAME>
----------------------------*/
revoke <NAME_ROLE>; to <USER_NAME>;
----------------------------


begin
for c in (select table_name, owner from dba_tables where owner = '<USER_NAME>')
  loop
    begin
      execute immediate
              'GRANT SELECT ON '||c.owner||'.' || c.table_name || ' to <NAME_ROLE> ';
    end;
  end loop;
end;

