/* Size and free space for all table spaces*/

-- 1 variant 
SELECT a.tablespace_name, "Free, MB", "Total, MB" FROM
  (SELECT tablespace_name, ROUND(SUM(bytes)/1024/1024) AS "Total, MB" FROM dba_data_files GROUP BY tablespace_name
  UNION
  SELECT tablespace_name, ROUND(SUM(bytes)/1024/1024) AS "Total, MB" FROM dba_temp_files GROUP BY tablespace_name) a,
  (SELECT tablespace_name, ROUND(SUM(bytes)/1024/1024) AS "Free, MB" FROM dba_free_space GROUP BY tablespace_name) b
WHERE a.tablespace_name = b.tablespace_name (+)
ORDER BY a.tablespace_name;

-- 2 variant query
select 	a.TABLESPACE_NAME tablespace_name, b.BYTES total_bytes, a.BYTES free_bytes,
        round(a.BYTES*100/b.BYTES,2) percent_free,
        round((b.BYTES-a.BYTES)*100/b.BYTES,2) percent_used
from  (select TABLESPACE_NAME, sum(BYTES) BYTES from dba_free_space group by TABLESPACE_NAME) a,
      (select TABLESPACE_NAME, sum(BYTES) BYTES from dba_data_files group by TABLESPACE_NAME) b
where a.TABLESPACE_NAME=b.TABLESPACE_NAME
order by a.TABLESPACE_NAME;


/* The total size of all tablespace's */
SELECT ROUND(SUM(bytes)/1024/1024) AS "Total, MB"
FROM dba_data_files;


/* The user name, the path to the datafile, the status, the size of the table space for the user in the database*/
SELECT tablespace_name, file_name, status, bytes
FROM dba_data_files
WHERE tablespace_name LIKE 'USERS'
ORDER BY tablespace_name, file_name;


select *
from dba_objects
where OBJECT_NAME LIKE '%APEX%';
