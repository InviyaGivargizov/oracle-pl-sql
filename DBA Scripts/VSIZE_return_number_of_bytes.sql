/* VSIZE  ���������� ����� � ������*/

/*VSIZE returns the number of bytes in the internal representation of expr. 
If expr is null, then this function returns null.

This function does not support CLOB data directly. 
However, CLOBs can be passed in as arguments through implicit data conversion.*/
  
  
  select vsize(to_char(1,'9')) bytes 
  from dual;
  
  select vsize('1') bytes 
  from dual;
  
  select vsize(1) bytes 
  from dual;
  
  select vsize(sysdate) bytes 
  from dual;  
