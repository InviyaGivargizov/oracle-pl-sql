--CREATE OR REPLACE PACKAGE test_easyAdmin IS
--Package for more easily daily administration of Oracle DB

--------------------------
  TYPE my_grantee IS RECORD(
     NAME_ROLE dba_tab_privs.grantee%TYPE,
     OWNER dba_tab_privs.owner%TYPE,
     TABLE_NAME dba_tab_privs.table_name%TYPE,
     PRIVILEGE dba_tab_privs.privilege%TYPE,
     TYPE_OBJ dba_tab_privs.type%TYPE
  );
  
  TYPE my_tabl IS RECORD(
     TABLE_NAME dba_tables.table_name%TYPE,
     OWNER dba_tables.owner%TYPE
  );
  
  TYPE my_tablspace IS RECORD(
     TABLESPACE_NAME dba_free_space.tablespace_name%TYPE,
     FREE_MB dba_free_space.bytes%TYPE,
     TOTAL_MB dba_data_files.bytes%TYPE,
     MAX_MB dba_data_files.maxbytes%TYPE
  );
  
  TYPE my_session IS RECORD(        
       SID varchar2(5),
       SER# varchar2(5),
       ORAUSER varchar2(10),
       OSUSER varchar2(10),
       PROGRAM varchar2(25),
       S varchar2(1),
       COMMAND varchar2(15),
       MACHINE varchar2(20),
       LOGON varchar2(15)
  );
  
--------------------------  
TYPE my_table_grantee IS TABLE OF my_grantee;
TYPE my_table_tabl IS TABLE OF my_tabl;
TYPE my_table_tablspace IS TABLE OF my_tablspace;
TYPE my_table_session IS TABLE OF my_session;


---- Group of administartion access rights
----------------------------------------------


--Search of existing roles 
FUNCTION searchRole(whatSearch in varchar2)
         RETURN my_table_grantee PIPELINED;

--Search of existing tables and owners         
FUNCTION searchTables(whatSearch in varchar2)
         RETURN my_table_tabl PIPELINED;

--Search active sessions and information about this sessions      
FUNCTION searchSession(whatSearch in varchar2) 
         RETURN my_table_session PIPELINED;      

--Give tables to role
PROCEDURE giveSelectToRole(whatOwner in varchar2 default null, 
                           whatTableName in varchar2 default null, 
                           whatRoleName in varchar2);

--Give roles to user
PROCEDURE giveRoleToUser(whatRole in varchar2, 
                         whatUser in varchar2);
                         
                         
--Take away roles from user
PROCEDURE takeAwayRoleFrmUser(whatRole in varchar2, 
                              whatUser in varchar2);


---- Group of administartion tablespaces and memory
-------------------------------------------------------

--Search of all tablespaces in DB
FUNCTION searchTablespace(whatTablespace in varchar2)
         RETURN my_table_tablspace PIPELINED;


----                         
END test_easyAdmin;
/
CREATE OR REPLACE PACKAGE BODY test_easyAdmin IS

---- Group of administartion access rights
----------------------------------------------

--Search of existing roles 
FUNCTION searchRole(whatSearch in varchar2) 
 RETURN my_table_grantee PIPELINED IS

BEGIN

     FOR v_rec IN (SELECT DISTINCT(grantee),owner,table_name,privilege,type 
                  FROM dba_tab_privs 
                  WHERE grantee LIKE UPPER('%'||whatSearch||'%') 
                        OR owner LIKE UPPER('%'||whatSearch||'%')
                        OR table_name LIKE UPPER('%'||whatSearch||'%')
                        OR type LIKE UPPER('%'||whatSearch||'%')
                     )
     LOOP
           PIPE ROW(v_rec);
      END LOOP;

      RETURN;
END searchRole;


--Search of existing tables and owners
FUNCTION searchTables(whatSearch in varchar2) 
 RETURN my_table_tabl PIPELINED IS

BEGIN

     FOR v_rec IN (SELECT table_name, owner
                  FROM dba_tables 
                  WHERE table_name LIKE UPPER('%'||whatSearch||'%')
                        OR owner LIKE UPPER('%'||whatSearch||'%')
                     )
     LOOP
           PIPE ROW(v_rec);
      END LOOP;

      RETURN;
END searchTables;




FUNCTION searchSession(whatSearch in varchar2) 
 RETURN my_table_session PIPELINED IS

BEGIN            
     FOR v_rec IN (
       SELECT lpad(to_char(s.sid), 5) sid,
              lpad(to_char(decode(sign(s.serial#),-1,65536 + s.serial#,s.serial#)),5) ser#,
              substr(s.username, 1, 10) orauser,
              substr(s.osuser, 1, 10) osuser,
              nvl(rpad(substr(upper(s.program),instr(s.program, '\', -1, 1) + 1,length(s.program)),25),'NOT DEFINED') program,
              substr(s.status, 1, 1) S,
              substr(a.name, 1, 15) command,
              substr(s.machine, instr(s.machine, '\', 1, 1) + 1, length(s.machine)) machine,
              to_char(logon_time, 'dd/mm hh24:mi') logon
  FROM v$session s, sys.audit_actions a
  WHERE a.action = s.command
   AND status = 'ACTIVE'
   AND username LIKE UPPER('%'||whatSearch||'%')
       OR username LIKE LOWER('%'||whatSearch||'%')
       /*OR osuser LIKE UPPER('%'||whatSearch||'%')
       OR osuser LIKE LOWER('%'||whatSearch||'%')*/
       ORDER BY logon
       )
     LOOP
           PIPE ROW(v_rec);
      END LOOP;

      RETURN;
END searchSession;    


--Bring tables to role
PROCEDURE giveSelectToRole(whatOwner in varchar2 default null, 
                    whatTableName in varchar2 default null, 
                    whatRoleName in varchar2) IS

t_tab dba_tables.owner%TYPE;

BEGIN

IF (whatOwner IS NULL AND whatTableName IS NOT NULL) THEN
  SELECT owner INTO t_tab
        FROM dba_tables
        WHERE table_name = whatTableName;
        BEGIN
           EXECUTE IMMEDIATE
              'GRANT SELECT ON '||t_tab||'.' || whatTableName || ' to '|| whatRoleName;
              END;
              DBMS_OUTPUT.PUT_LINE('������ ���� '|| whatRoleName ||' ����� �� SELECT ������� '||t_tab||'.'||whatTableName);
              
ELSIF (whatOwner IS NOT NULL AND whatTableName IS NULL) THEN
  FOR v_rec IN (SELECT table_name, owner FROM dba_tables WHERE owner = whatOwner)
   LOOP
     BEGIN
      EXECUTE IMMEDIATE
              'GRANT SELECT ON '||whatOwner||'.' || v_rec.table_name || ' to '|| whatRoleName;
          END;
          DBMS_OUTPUT.PUT_LINE('������ ���� '|| whatRoleName ||' ����� �� SELECT ������� '||whatOwner||'.'||whatTableName);
              END LOOP;
  END IF;
END giveSelectToRole;


--Give roles to user
PROCEDURE giveRoleToUser(whatRole in varchar2, 
                         whatUser in varchar2) IS
 BEGIN
    EXECUTE IMMEDIATE
            'GRANT'|| whatRole ||' TO '|| whatUser;
    DBMS_OUTPUT.PUT_LINE('������ ������������ '|| whatUser ||' ���� '||whatRole);
             
END giveRoleToUser;


--Take away roles from user
PROCEDURE takeAwayRoleFrmUser(whatRole in varchar2, 
                         whatUser in varchar2) IS
 BEGIN
    EXECUTE IMMEDIATE
            'REVOKE'|| whatRole ||' TO '|| whatUser;
    DBMS_OUTPUT.PUT_LINE('������� � ������������ '|| whatUser ||' ���� '||whatRole);
             
            END takeAwayRoleFrmUser;          

            
---- Group of administartion tablespaces and memory
-------------------------------------------------------


--Search of all tablespaces in DB
FUNCTION searchTablespace(whatTablespace in varchar2) 
 RETURN my_table_tablspace PIPELINED IS

BEGIN

     FOR v_rec IN (SELECT a.tablespace_name tablespace_name, a.bytes free_mb, b.bytes total_mb, b.max_mb
                   FROM (SELECT tablespace_name, ROUND(SUM(bytes)/1024/1024) bytes FROM dba_free_space WHERE tablespace_name LIKE UPPER('%'||whatTablespace||'%') OR tablespace_name LIKE LOWER('%'||whatTablespace||'%') GROUP BY tablespace_name) a,
                        (SELECT tablespace_name, ROUND(SUM(bytes)/1024/1024) bytes, ROUND(SUM(maxbytes)/1024/1024) max_mb FROM dba_data_files GROUP BY tablespace_name,autoextensible) b
                   WHERE a.tablespace_name = b.tablespace_name
                    AND a.tablespace_name not in ('SYSAUX')
                   ORDER BY  b.bytes)
     LOOP
           PIPE ROW(v_rec);
      END LOOP;

      RETURN;
END searchTablespace;


----
END test_easyAdmin;
/
