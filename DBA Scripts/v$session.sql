-----------------
select lpad(to_char(s.sid),5) sid,
      lpad(to_char(decode(sign(s.serial#), -1,
         65536+s.serial#, s.serial# )), 5) ser#,
      substr(s.username,1,10) orauser,
      substr(s.osuser,1,10) osuser,
      nvl(rpad(substr(upper(s.program),instr(s.program,'\',-1,1)+1,
      length(s.program)),25),'NOT DEFINED') program,
      substr(s.status,1,1) S,
      substr(a.name,1,15) command,
      substr(s.machine,instr(s.machine,'\',1,1)+1,
      length(s.machine)) machine,
      to_char(logon_time,'dd/mm hh24:mi') logon
  from v$session s, sys.audit_actions a
  where a.action = s.command
  and status = 'ACTIVE'
  order by logon;
----------------------
select lpad(to_char(s.sid), 5) sid,
       lpad(to_char(decode(sign(s.serial#),
                           -1,
                           65536 + s.serial#,
                           s.serial#)),
            5) ser#,
       substr(s.username, 1, 10) orauser,
       substr(s.osuser, 1, 10) osuser,
       nvl(rpad(substr(upper(s.program),
                       instr(s.program, '\', -1, 1) + 1,
                       length(s.program)),
                25),
           'NOT DEFINED') program,
       substr(s.status, 1, 1) S,
       substr(a.name, 1, 15) command,
       substr(s.machine, instr(s.machine, '\', 1, 1) + 1, length(s.machine)) machine,
       to_char(logon_time, 'dd/mm hh24:mi') logon
  from v$session s, sys.audit_actions a
 where a.action = s.command
   and status = 'ACTIVE'
 order by logon;
-----------------   
SELECT sysdate,a.username, a.sid, a.serial#, a.osuser, (b.blocks*d.block_size)/1048576 MB_used, c.sql_text
FROM v$session a, v$tempseg_usage b, v$sqlarea c,
     (select block_size from dba_tablespaces where tablespace_name='TEMP') d
    WHERE b.tablespace = 'TEMP'
    and a.saddr = b.session_addr
    AND c.address= a.sql_address
    AND c.hash_value = a.sql_hash_value
    AND (b.blocks*d.block_size)/1048576 > 1024
    ORDER BY b.tablespace, 6 desc;
-----------------    
SELECT S.sid || ',' || S.serial# sid_serial,
       S.username,
       S.osuser,
       P.spid,
       S.module,
       P.program,
       SUM(T.blocks) * TBS.block_size / 1024 / 1024 mb_used,
       T.tablespace,
       COUNT(*) statements
  FROM v$sort_usage T, v$session S, dba_tablespaces TBS, v$process P
 WHERE T.session_addr = S.saddr
   AND S.paddr = P.addr
   AND T.tablespace = TBS.tablespace_name
 GROUP BY S.sid,
          S.serial#,
          S.username,
          S.osuser,
          P.spid,
          S.module,
          P.program,
          TBS.block_size,
          T.tablespace
 ORDER BY mb_used;
