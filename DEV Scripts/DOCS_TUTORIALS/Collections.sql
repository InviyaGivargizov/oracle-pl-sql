CREATE TYPE first_names_t IS VARRAY (2) OF VARCHAR2 (100);
/
CREATE TYPE child_names_t IS VARRAY (10) OF VARCHAR2 (100);
/
CREATE TABLE family (
surname VARCHAR2(1000)
, parent_names first_names_t
, children_names child_names_t
);
/
DECLARE
  parents first_names_t := first_names_t ();
  children child_names_t := child_names_t ();
BEGIN
  parents.EXTEND (2);
  parents (1) := 'Samuel';
  parents (2) := 'Charina';
--
  children.EXTEND (10);
  children (1) := 'Feather1';
  children (2) := 'Feather2';
  children (3) := 'Feather3';
  children (4) := 'Feather4';
  children (5) := 'Feather5';
  children (6) := 'Feather6';
  children (7) := 'Feather7';

 --
 INSERT INTO family
        ( surname, parent_names, children_names )
 VALUES ( 'Assurty', parents, children );
 END;
 
 drop table apex_040000.family
 
 select * from family 
 for update
 
SELECT COLUMN_VALUE a,  t.rowid
FROM TABLE (UPDATE family
       SET children_names := 'children_names'
        WHERE surname='Assurty2') ;
 
SELECT COLUMN_VALUE a
       FROM TABLE (SELECT children_names
       FROM family 
       WHERE surname='Assurty3')

/
SELECT COLUMN_VALUE a
       FROM TABLE (SELECT parent_names
       FROM family
       WHERE surname='Assurty3')      
----------------------------
DECLARE
    children family.children_names :=child_names_t;
BEGIN
  
children.EXTEND(10);
children(children.LAST) := 'DONE';

    
 UPDATE family
     SET children_names = children
 WHERE surname='Assurty1';

 
FOR rec IN ( SELECT COLUMN_VALUE children_names
             FROM TABLE (SELECT children_names
       FROM family
       WHERE surname='Assurty1'))
LOOP
     DBMS_OUTPUT.put_line (rec.children_names);
END LOOP;
end;


CREATE OR REPLACE PROCEDURE pushval (the_list IN OUT children_names_t, new_value IN VARCHAR2)
          AS
BEGIN
the_list.EXTEND;
the_list(the_list.LAST) := new_value;
END;


begin
  <FUNCT>@<REMOTE>(2,'SECOND_TEST');
  
end;

BEGIN
  apex_040000.pushval(family.children_names, 'HI');
  END;
