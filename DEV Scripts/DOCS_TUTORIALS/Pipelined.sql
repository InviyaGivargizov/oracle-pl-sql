/*Pipelined-function*/
create table TableTestPipelined 
(
       object_name varchar2(500),
       object_id  number,
       object_type varchar2(10)
);

/*Preparing data types*/
create or replace type TypeTestPipelined as object (
       object_name varchar2(500),
       object_id  number,
       object_type varchar2(10)
       )
       
/*Create collection type nested table*/
create type TypeObjectTestPipelined as table of TypeTestPipelined;

/*Create function*/
create or replace function TestFunctionPipelined(pObject_type in varchar2)
          return TypeObjectTestPipelined pipelined as
begin
  for i in (
      select tao.OBJECT_NAME, tao.OBJECT_ID, tao.OBJECT_TYPE
        from all_objects tao
       where tao.OBJECT_TYPE=pObject_type
     )
 loop
   pipe row (TypeTestPipelined(i.OBJECT_NAME, i.OBJECT_ID, i.OBJECT_TYPE));    
  end loop;
 return;
end;


/* Starts */
select *
 from table(TestFunctionPipelined('TABLE')) t
where t.object_id=20;


SELECT *
FROM ALL_OBJECTS

insert into tabletestpipelined (object_name,object_id,object_type)
select * from table(TestFunctionPipelined('TABLE'))

/* The procedure that inserts into tabletestpipelined */
create or replace procedure TestProcedurePipelined(pObject_type in varchar2) IS
       CURSOR get_obj IS select * into a from ALL_OBJECTS
                         where OBJECT_TYPE=pObject_type;

begin
  select * into a from ALL_OBJECTS
         where OBJECT_TYPE=pObject_type;
 
         FOR a IN get_obj LOOP      
 	           INSERT INTO tabletestpipelined(object_name, object_id, object_type)
		         VALUES(a.object_name, a.object_id, a.object_type);
             END LOOP;
     commit;
    end;
    
/* Starts pipelined */ 
insert into tabletestpipelined (object_name,object_id,object_type)
select * from table(TestFunctionPipelined('TABLE'));

/* Starts ��������� */
BEGIN 
  testprocedurepipelined('TABLE');
  END;
