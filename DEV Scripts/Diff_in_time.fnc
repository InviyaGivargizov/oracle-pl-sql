CREATE OR REPLACE FUNCTION diff_in_time(TIME_1 IN VARCHAR2,
                                        TIME_2 IN VARCHAR2) RETURN VARCHAR2 IS
  /* Function that returns the time difference betweenTIME_1 � TIME_2 */
BEGIN

  RETURN EXTRACT(HOUR FROM NUMTODSINTERVAL(TO_DATE(TIME_1, 'HH24-MI') -
                                 TO_DATE(TIME_2, 'HH24-MI'),
                                 'DAY')) || ':' || EXTRACT(MINUTE FROM
                                                           NUMTODSINTERVAL(TO_DATE(TIME_1,
                                                                                   'HH24-MI') -
                                                                           TO_DATE(TIME_2,
                                                                                   'HH24-MI'),
                                                                           'DAY'));
END diff_in_time;
/

