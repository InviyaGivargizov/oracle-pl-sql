function function_with_pipelined_return (p_date in date) RETURN datalist_TABLE PIPELINED AS

  recs datalist_table;
  
  from_date <COLUMN>.<TABLE>%TYPE;
  query  VARCHAR2(32000);
  
BEGIN

  SELECT MAX(FIN_DATE) INTO from_date ---������� ����. ������� ����
         FROM <TABLE>
         WHERE <COLUMN> < p_date AND STATUS=0; 
     
  query := q'[SELECT  *
                        FROM <TABLE> f
                        WHERE NOT EXISTS (SELECT <COLUMN>
                                FROM <TABLE> t
                                WHERE <COLUMN>='DEPOSIT'
                                     AND <COLUMN>=:1
                                     AND f.<COLUMN> = t.<COLUMN>)
                                     AND <COLUMN>=:2
                                     AND <COLUMN>='DEPOSIT']'; --������ �� ����� ���������� �������� ��������
              
  EXECUTE IMMEDIATE query BULK COLLECT
      INTO recs
      USING p_date, from_date; 
            
  FOR i IN 1 .. recs.count LOOP

    PIPE ROW(datalist_TYPE(recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>,
                           recs(i).<COLUMN>));
         END LOOP;
  RETURN;
END;
