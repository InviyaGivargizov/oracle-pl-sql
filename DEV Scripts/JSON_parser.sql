/* Parser for JSON objects can parse simple JSON structure like {"firstName": "��������","lastName": "�������","Address": "��.������� 24", "postalCode": 11000  } */
DECLARE
  json_input varchar2(1500) := '&jsonInput';
  jsn_cntObj number;
  jsn_cntArr number;
  
  json_outputObj varchar2(1000);    
  json_outputArr varchar2(1000);
  
BEGIN
  jsn_cntObj := REGEXP_COUNT(json_input, '("|\s|[:digit:])([[:alnum:]|[:digit:]])+(.)(:)');
  jsn_cntArr := REGEXP_COUNT(json_input, '("|[[:alnum:]|[:digit:]])([[:alnum:]|[:digit:]])([[:alnum:]|[:digit:]])+("|\s|)(,|\s|})');
                                        
  
  FOR i IN 1 .. jsn_cntObj LOOP             
    json_outputObj := REGEXP_SUBSTR(REGEXP_SUBSTR(json_input, '("|\s|[:digit:])([[:alnum:]|[:digit:]])+(.)(:)',1,i), '([[:alnum:]|[:digit:]])([[:alnum:]|[:digit:]])+') ||' '|| json_outputObj;
    END LOOP;                                                                                                     

  FOR i IN 1 .. jsn_cntArr LOOP
    json_outputArr := REGEXP_SUBSTR(REGEXP_SUBSTR(json_input, '("|[[:alnum:]|[:digit:]])([[:alnum:]|[:digit:]|\s|.|,])([[:alnum:]|[:digit:]])+("|\s|)(,|\s|})',1,i), '([[:alnum:]|[:digit:]])([[:alnum:]|[:digit:]])+') ||' '|| json_outputArr;
    END LOOP;
    dbms_output.put_line('---------');
    dbms_output.put_line(json_outputObj);
    dbms_output.put_line(json_outputArr);
    dbms_output.put_line('---------');
    
  END;
