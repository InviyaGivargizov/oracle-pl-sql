/*Output result from apex through dblink */

declare
  x varchar2(1000);
  status int;
  a number(2) := 6;
  b varchar2(20) :='TEST_DONE';
begin
  dbms_output.enable@<DBLINK>;  

  <PACKAGE>.<FUNCTION>@<DBLINK>(a, b);
  
  loop
    dbms_output.get_line@<DBLINK>( x,status);
    exit when status != 0;
    dbms_output.put_line(x);
  end loop;
end;
/

/* Second option (variant) */
DECLARE
   v_rec_sample  <TABLE>@<DBLINK>%ROWTYPE;
   id_from number(2) := 2;
   name_from varchar2(20) := 'SECOND_TEST';
BEGIN
   
   <PACKAGE>.<FUNCTION>@<DBLINK>(id_from,name_from); 

   select *
     into v_rec_sample
     from <TABLE>@<DBLINK>
    where id = id_from;

   dbms_output.put_line(v_rec_sample.<COLUMN>||' '||v_rec_sample.<COLUMN>);

END;
