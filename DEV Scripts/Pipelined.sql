type r_instant is record ( productCode w4_product.code%type,
                           productName w4_product.name%type,
                           CardCode v_w4_card.code%type,
                           CardName v_w4_card.sub_name%type,
                           KV tabval.lcv%type);
                           
type t_instant is table of r_instant;


function getInstantDict return t_instant pipelined
is
l_instant r_instant;
begin
for k in (select wp.code productCode, wp.name productName, vc.code CardCode, vc.sub_name CardName, t.lcv KV
from w4_product wp, v_w4_card vc, tabval t
where grp_code = 'INSTANT' and nvl(date_close, gl.bd + 1) > gl.bd
and vc.product_code = wp.code
and wp.kv = t.kv
order by wp.code)
loop
l_instant.productCode := k.productCode;
l_instant.productName := k.productName;
l_instant.CardCode := k.CardCode;
l_instant.CardName := k.CardName;
l_instant.KV := k.KV;

pipe row(l_instant);
end loop;

end;
