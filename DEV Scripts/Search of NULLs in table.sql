declare            
       v_rec dba_tables%rowtype;
       b_rec dba_tab_columns%rowtype;
       
       whatSearch varchar2(40) := '&whatSearch';
       
       selectStat varchar2(9000);
       nameTblClmn varchar2(100);
       resultTable varchar2(100);
       resultColumns varchar2(2000);
       resultEnd varchar2(2000);
       res varchar2(2000);
begin
  
  ----
  --Search table
     FOR v_rec IN (SELECT table_name, owner
                  FROM dba_tables 
                  WHERE table_name LIKE UPPER('%'||whatSearch||'%')
                        OR owner LIKE UPPER('%'||whatSearch||'%')
                     )
     LOOP
         resultTable := v_rec.table_name;
         dbms_output.put_line('--> '||resultTable);
         
    ----
    --Search in column 
      FOR b_rec IN (SELECT table_name, column_name, owner
                           FROM dba_tab_columns 
                           WHERE table_name = resultTable)
     LOOP
       
     nameTblClmn := b_rec.table_name||'.'||b_rec.column_name;
     selectStat := 'SELECT rownum, '''||nameTblClmn||''', CASE WHEN '||b_rec.column_name||' IS NULL THEN 0 ELSE 1 END FROM '||b_rec.owner||'.'||b_rec.table_name||' WHERE '||b_rec.column_name||' IS NULL';
                  
           EXECUTE IMMEDIATE 'INSERT INTO HR.TMP_OBJ(rownumber,tblClmn, checks) '||selectStat; 
           EXECUTE IMMEDIATE 'DELETE FROM HR.TMP_OBJ WHERE checks=1';

           commit;
      END LOOP;  

   END LOOP;
   
    ----
    --Output results   
         FOR n_rec IN (SELECT rownumber, checks, tblClmn FROM HR.TMP_OBJ WHERE checks=0)
                 LOOP 
                         DBMS_OUTPUT.PUT_LINE('There is NULL on '||n_rec.rownumber||' row, table '||n_rec.tblClmn);
                   END LOOP;
   
END;
