CREATE TABLE TEST_BANK_ACC (ID NUMBER PRIMARY KEY, 
                                   FIRSTNAME VARCHAR2(128), 
																	 SURNAME VARCHAR2(128), 
																	 BANKACCOUNT NUMBER(20), 
																	 DEBIT NUMBER(20),
																	 CREDIT NUMBER(20),
																	 CMNT VARCHAR(200));
																	 
																	 
ALTER TABLE TEST_BANK_ACC ADD(CURR VARCHAR2(10));
ALTER TABLE TEST_BANK_ACC ADD(BAL NUMBER(20));
---------------------------------------------------------------------------------------------------------																 
COMMENT ON TABLE TEST_BANK_ACC IS '�������� ������� ��� �������� � ������������� � �������� �������� ������ ��� ��������, �������, ���������';
---------------------------------------------------------------------------------------------------------
COMMENT ON COLUMN TEST_BANK_ACC.ID IS 'unique number of the contragent';
COMMENT ON COLUMN TEST_BANK_ACC.BANKACCOUNT IS 'bank account of the contragent';
COMMENT ON COLUMN TEST_BANK_ACC.FIRSTNAME IS 'firstname of the contragent or manager of this contragent';
COMMENT ON COLUMN TEST_BANK_ACC.SURNAME IS 'surname of the contragent or manager of this contragent';
COMMENT ON COLUMN TEST_BANK_ACC.DEBIT IS 'credit value of the contragent';
COMMENT ON COLUMN TEST_BANK_ACC.CREDIT IS 'debit value of the contragent';
COMMENT ON COLUMN TEST_BANK_ACC.CMNT IS 'comments for any remark clarification of this contragent';
COMMENT ON COLUMN TEST_BANK_ACC.CURR IS 'Currency account';
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
SELECT *
FROM TEST_BANK_ACC
---------------------------------------------------------------------------------------------------------
SELECT B.ID, A.BANKACCOUNT, COUNT(*)
FROM TEST_BANK_ACC A, TEST_BANK_ACC B
WHERE A.ID = B.ID
GROUP BY A.BANKACCOUNT, A.ID
ORDER BY B.ID asc;



---------------------------------------------------------------------------------------------------------
UPDATE TEST_BANK_ACC
			       SET TEST_BANK_ACC.BANKACCOUNT = 1643806 
             WHERE TEST_BANK_ACC.BANKACCOUNT = 9910874 AND ID = 21
---------------------------------------------------------------------------------------------------------
DECLARE
     CURSOR insAcc IS SELECT BANKACCOUNT
		        FROM TEST_BANK_ACC;   

BEGIN
	FOR P IN insAcc LOOP
	    UPDATE TEST_BANK_ACC
			       SET TEST_BANK_ACC.BANKACCOUNT = 0;
	END LOOP;
	commit;
END;  
---------------------------------------------------------------------------------------------------------				
begin
	testBankAccDebitValue(1,10);
end;
---------------------------------------------------------------------------------------------------------
DECLARE
       CURSOR insAcc IS SELECT DEBIT, ID, BANKACCOUNT
							FROM TEST_BANK_ACC;
							
      VAL  TEST_BANK_ACC.DEBIT%TYPE :=1;
			IDACC TEST_BANK_ACC.ID%TYPE :=1;
			K NUMBER :=0;
BEGIN
	
  IF (IDACC = NULL) THEN
  FOR i IN insAcc LOOP
			k := K+1;				
				  UPDATE TEST_BANK_ACC
             SET TEST_BANK_ACC.DEBIT = TEST_BANK_ACC.DEBIT + VAL
						 WHERE TEST_BANK_ACC.ID = K;
					
			DBMS_OUTPUT.PUT_LINE('Bank account: '||i.BANKACCOUNT||' is changed to '||VAL);
	END LOOP;
	
	ELSIF (IDACC != NULL) THEN
		    UPDATE TEST_BANK_ACC
             SET TEST_BANK_ACC.DEBIT = TEST_BANK_ACC.DEBIT + VAL
						 WHERE TEST_BANK_ACC.ID = IDACC;
			DBMS_OUTPUT.PUT_LINE('Bank account: '||TEST_BANK_ACC.BANKACCOUNT||' is changed to '||VAL);
			
	ELSE
		  DBMS_OUTPUT.PUT_LINE('Error!');
	END IF;
END;
---------------------------------------------------------------------------------------------------------

SELECT *
FROM TEST_BANK_ACC


DECLARE 
      CURSOR insName IS SELECT FIRSTNAME,SURNAME,ID
			       FROM TEST_BANK_ACC;
			K NUMBER(1);
			K1 NUMBER(1);
			K2 NUMBER(1);
BEGIN 
	    IF (K = 0  
