CREATE OR REPLACE PROCEDURE TESTBANKACCADDCONTRAGENT (FirstName_Par IN VARCHAR2, 
                                                            SurName_Par IN VARCHAR2, 
																														Debit_Par IN NUMBER,
																														Credirt_Par IN NUMBER,
																														Cmnt_Par IN VARCHAR,
																														Curr_Par IN VARCHAR2) IS

BEGIN 

      INSERT INTO TEST__BANK_ACC
			       (ID, FIRSTNAME, SURNAME, BANKACCOUNT,BAL, DEBIT, CREDIT, CMNT, CURR)
			VALUES
			       (TESTID.NEXTVAL,FirstName_Par, SurName_Par, TO_NUMBER('2620'||TO_CHAR(TESTID.CURRVAL)),Debit_Par-Credirt_Par, Debit_Par, Credirt_Par, Cmnt_Par, Curr_Par);
			
				
	--UPDATE TEST__BANK_ACC
	     --SET TEST__BANK_ACC.BAL= TEST__BANK_ACC.DEBIT - TEST__BANK_ACC.CREDIT;

			DBMS_OUTPUT.PUT_LINE('Create contragent: '||TESTID.CURRVAL);
			DBMS_OUTPUT.PUT_LINE('Firstname: '||FirstName_Par);
			DBMS_OUTPUT.PUT_LINE('Surname: '||SurName_Par);
			DBMS_OUTPUT.PUT_LINE('With # account: '||TO_NUMBER('2620'||TO_CHAR(TESTID.CURRVAL)));
			DBMS_OUTPUT.PUT_LINE('With debit: '||Debit_Par);
			DBMS_OUTPUT.PUT_LINE('With credit: '||Credirt_Par);
			DBMS_OUTPUT.PUT_LINE('Currency: '||Curr_Par);
			DBMS_OUTPUT.PUT_LINE('With comments: '||Cmnt_Par);
			
END TESTBANKACCADDCONTRAGENT;
/
