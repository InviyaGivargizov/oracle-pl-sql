CREATE OR REPLACE PROCEDURE "TEST_BANKACCCREDITVALUE" (VAL IN NUMBER, IDACC IN NUMBER)
is
       /*��������� ������� ����������� ��� ��������� ���� ������,
       VAL - �� ����� ���������� ����������� ��� ��������� ����
       IDACC - ����� ID �������, ���� � IDACC ������ 0 - ����������� �� ���� ������
       */  
       
       CURSOR insAcc IS SELECT CREDIT, ID, BANKACCOUNT, CURR
              FROM TEST_BANK_ACC;
      
       CURSOR insBankid IS SELECT BANKACCOUNT,CURR
              FROM TEST_BANK_ACC
              WHERE ID=IDACC;                 
       
       C TEST_BANK_ACC.CURR%TYPE; -- ������ ������������
       O TEST_BANK_ACC.BANKACCOUNT%TYPE; -- ��������������� �������. ��� ����������� ����� ��������. ���� IDACC != 0
       K NUMBER :=0;                            -- �������. ��������

BEGIN
	
  IF (IDACC = 0) THEN                         --���� �� ������ ����� ID �������
  FOR i IN insAcc LOOP
      k := K+1;			
		UPDATE TEST_BANK_ACC
				  SET TEST_BANK_ACC.DEBIT = TEST_BANK_ACC.DEBIT + VAL
					WHERE TEST_BANK_ACC.ID = 55 AND TEST_BANK_ACC.BANKACCOUNT = 11011;  --������ ������� �� ����� 11011(���� �����)
			     
			        
    UPDATE TEST_BANK_ACC
          SET TEST_BANK_ACC.CREDIT = TEST_BANK_ACC.CREDIT + VAL
          WHERE TEST_BANK_ACC.ID = K;                                                --���������� ������� �� ���� ������� K 
						 		    
			    
    INSERT INTO TEST_BANK_TRANSACT (ID,BANKACCOUNT_A,BANKACCOUNT_B,AMOUNT,CURR,CMNT,DATE_TRANSC)
          VALUES(TEST_IDTRANSACT.NEXTVAL, 
					       11011, 
								 i.BANKACCOUNT, 
								 VAL, 
								 I.CURR,
								 '������������ ������� �� ����� 11011 �� ���� '||i.BANKACCOUNT, 
								 TO_CHAR(SYSTIMESTAMP,'HH:MI:SS - DD.MM.YYYY'));                            --������ � ������ ����������
                   
  END LOOP;
  
  ELSIF (IDACC != 0) THEN  --���� ������ ����� ID �������    
	     												 
	IF (IDACC = 55) THEN             --���� ������ ����� ID �����
		
		UPDATE TEST_BANK_ACC
				  SET TEST_BANK_ACC.CREDIT = TEST_BANK_ACC.CREDIT + VAL
					WHERE TEST_BANK_ACC.ID = 55 AND TEST_BANK_ACC.BANKACCOUNT = 11011;  --���������� ������� �� ���� ������� K 												 


 OPEN insBankid;
 FETCH insBankid INTO O,C;				 
           
    INSERT INTO TEST_BANK_TRANSACT (ID,BANKACCOUNT_A,BANKACCOUNT_B,AMOUNT,CURR,CMNT,DATE_TRANSC)
          VALUES(TEST_IDTRANSACT.NEXTVAL, 
							   11011, 
								 O, 
								 VAL, 
								 C,
								 '���������� ������� ������ �� ���� '||O, 
								 TO_CHAR(SYSTIMESTAMP,'HH:MI:SS - DD.MM.YYYY'));                            --������ � ������ ����������

 CLOSE insBankid;
 
			GOTO IDACC55END;                                                                      --���� ��������� ���������� ���� � ����� �� �����
			
	END IF;
																 
			UPDATE TEST_BANK_ACC
				    SET TEST_BANK_ACC.DEBIT = TEST_BANK_ACC.DEBIT + VAL
						WHERE TEST_BANK_ACC.ID = 55 AND TEST_BANK_ACC.BANKACCOUNT = 11011;  --������ ������� �� ����� 11011(���� �����)  												 
				
      UPDATE TEST_BANK_ACC
            SET TEST_BANK_ACC.CREDIT = TEST_BANK_ACC.CREDIT + VAL
            WHERE TEST_BANK_ACC.ID = IDACC;                                            --���������� ������� �� ���� ������� K 
             
      OPEN insBankid;
        FETCH insBankid INTO O,C;
				   
      INSERT INTO TEST_BANK_TRANSACT (ID,BANKACCOUNT_A,BANKACCOUNT_B,AMOUNT,CURR,CMNT,DATE_TRANSC)
            VALUES(TEST_IDTRANSACT.NEXTVAL, 
							     11011, 
									 O, 
									 VAL, 
									 C,
									 '������������ ������� �� ����� 11011 �� ���� '||O, 
									 TO_CHAR(SYSTIMESTAMP,'HH:MI:SS - DD.MM.YYYY'));                            --������ � ������ ����������             

 CLOSE insBankid;
 
  ELSE                                       --���������� ���������� ������
      DBMS_OUTPUT.PUT_LINE('Error!');
  END IF;
	
  <<IDACC55END>>                             
	
  COMMIT;
end TEST_BANKACCCREDITVALUE;
/
