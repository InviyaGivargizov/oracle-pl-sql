create or replace procedure TEST_BANKACC_UPDATE(ID IN NUMBER,
                                                     FIRSTNAME IN VARCHAR2,
                                                     SURNAME IN VARCHAR2,
																										 BANKACCOUNT IN NUMBER,
                                                     DEBIT IN NUMBER,
                                                     CREDIT IN NUMBER,
                                                     CMNT IN VARCHAR2,
                                                     CURR IN VARCHAR2) IS
BEGIN
      UPDATE TEST_BANK_ACC 
             SET FIRSTNAME = TEST_BANKACC_UPDATE.FIRSTNAME,
						     SURNAME = TEST_BANKACC_UPDATE.SURNAME,
								 BANKACCOUNT = TEST_BANKACC_UPDATE.BANKACCOUNT,
								 DEBIT = TEST_BANKACC_UPDATE.DEBIT,
								 CREDIT = TEST_BANKACC_UPDATE.CREDIT,
								 CMNT = TEST_BANKACC_UPDATE.CMNT,
								 CURR = TEST_BANKACC_UPDATE.CURR
						 WHERE ID = TEST_BANKACC_UPDATE.ID;

END TEST_BANKACC_UPDATE;
/
