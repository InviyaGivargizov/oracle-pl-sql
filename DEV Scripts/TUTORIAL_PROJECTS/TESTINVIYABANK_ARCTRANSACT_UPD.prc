CREATE OR REPLACE PROCEDURE TEST_BANK_ARCTRANSACT_UPD (ID IN NUMBER,
                                                              BANKACCOUNT_A IN NUMBER,
                                                              BANKACCOUNT_B IN NUMBER,
                                                              AMOUNT IN NUMBER,
                                                              CURR IN VARCHAR2,
                                                              CMNT IN VARCHAR2,
                                                              DATE_TRANSC IN VARCHAR2) 
                                                          IS
                                                          
BEGIN
  UPDATE TEST_BANK_ARCTRANSACT 
             SET BANKACCOUNT_A = TEST_BANK_ARCTRANSACT_UPD.BANKACCOUNT_A,
                 BANKACCOUNT_B = TEST_BANK_ARCTRANSACT_UPD.BANKACCOUNT_B,
                 AMOUNT = TEST_BANK_ARCTRANSACT_UPD.AMOUNT,
                 CURR = TEST_BANK_ARCTRANSACT_UPD.CURR,
                 CMNT = TEST_BANK_ARCTRANSACT_UPD.CMNT,
                 DATE_TRANSC = TEST_BANK_ARCTRANSACT_UPD.DATE_TRANSC
						 WHERE ID = TEST_BANK_ARCTRANSACT_UPD.ID;
END TEST_BANK_ARCTRANSACT_UPD;
/
