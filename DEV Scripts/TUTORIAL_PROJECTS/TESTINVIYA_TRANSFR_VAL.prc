CREATE OR REPLACE PROCEDURE TEST_TRANSFR_VAL(VAL    IN NUMBER,
																									 IDACC  IN NUMBER,
																									 IDACC2 IN NUMBER) IS

	/* ��������� ������� ��������� ��������� �������� ��������(������������) � ����������� ������������ 
  � ������ ����������, ���� ������ ���������� � ����������� ����������. ��� �� ����� ����������� ���������� 
  ������� �� ��� ����� � ������� ��� ������ = ���� ������ ����������� ���� � ��������� IDACC2 ������ 0.
          TEST_TRANSFR_VAL([����� ��������], [��� �����������], [��� ����������] OR 
          [0(���� ���� ����������� � ���� ��� ������= ���� ������ �����������])*/

	CURSOR forAllAccounts IS
		SELECT ID, CURR, BANKACCOUNT
			FROM TEST_BANK_ACC
		 WHERE CURR IN (SELECT CURR FROM TEST_BANK_ACC WHERE ID = IDACC)
			 AND ID <> IDACC;

	BC NUMBER; -- ������ ������������(�� ����)
	BO NUMBER; -- ��������������� �������. ��� ����������� ����� ��������. ���� IDACC != 0(�� ����)
	BA VARCHAR2(50); -- �������� ������(�� ����)
	BN NUMBER; -- ����� �����(�� ����)

	BC2 NUMBER; -- ������ ������������(����)
	BO2 NUMBER; -- ��������������� �������. ��� ����������� ����� ��������. ���� IDACC != 0(����)
	BA2 VARCHAR2(50); -- �������� ������(����)
	BN2 NUMBER; -- ����� �����(����)

BEGIN
	IF (IDACC2 != 0) THEN
		--���� ���������� ���������� ������� ��������� ����������� � ��������� IDACC2
	
		SELECT C.NAMECURR
			INTO BA
			FROM TEST_BANK_CATCURR C, TEST_BANK_ACC A
		 WHERE C.CURRID = A.CURR
			 AND A.ID = IDACC;
	
		SELECT CURR INTO BC FROM TEST_BANK_ACC WHERE ID = IDACC;
	
		SELECT C.EXHGRATE
			INTO BO
			FROM TEST_BANK_CATCURR C, TEST_BANK_ACC A
		 WHERE C.CURRID = A.CURR
			 AND A.ID = IDACC;
	
		SELECT BANKACCOUNT INTO BN FROM TEST_BANK_ACC WHERE ID = IDACC;
		--------------------------------------------------------------------
		--------------------------------------------------------------------
		--------------------------------------------------------------------
		SELECT C.NAMECURR
			INTO BA2
			FROM TEST_BANK_CATCURR C, TEST_BANK_ACC A
		 WHERE C.CURRID = A.CURR
			 AND A.ID = IDACC2;
	
		SELECT CURR INTO BC2 FROM TEST_BANK_ACC WHERE ID = IDACC2;
	
		SELECT C.EXHGRATE
			INTO BO2
			FROM TEST_BANK_CATCURR C, TEST_BANK_ACC A
		 WHERE C.CURRID = A.CURR
			 AND A.ID = IDACC2;
	
		SELECT BANKACCOUNT
			INTO BN2
			FROM TEST_BANK_ACC
		 WHERE ID = IDACC2;
	
		IF (BC != BC2) THEN
			--���� ������ ����������� (�� ����) �� ����� ������ �����������(����) ---�������������� �����������---
		
			UPDATE TEST_BANK_ACC
				 SET TEST_BANK_ACC.DEBIT = TEST_BANK_ACC.DEBIT + VAL
			 WHERE TEST_BANK_ACC.ID = IDACC; --������ ������� �� ����� �����������                          
		
			UPDATE TEST_BANK_ACC
				 SET TEST_BANK_ACC.CREDIT = TEST_BANK_ACC.CREDIT +
																					 (TRUNC(VAL * (BO / BO2), 0))
			 WHERE TEST_BANK_ACC.ID = IDACC2; --���������� ������� �� ���� ������� ����������
		
			INSERT INTO TEST_BANK_TRANSACT
				(ID, BANKACCOUNT_A, BANKACCOUNT_B, AMOUNT, CURR, CMNT, DATE_TRANSC)
			VALUES
				(TEST_IDTRANSACT.NEXTVAL,
				 BN,
				 BN2,
				 (TRUNC(VAL * (BO / BO2), 0)),
				 BC || '>' || BC2,
				 '������������ ������� �� ����� ' || BN || ' �� ���� ' || BN2 ||
				 ', � �������������� ������� ����������� ������ � ' || BA || ' � ' || BA2 ||
				 ', �� ����� ' || BO || ' � ' || BO2,
				 TO_CHAR(SYSTIMESTAMP, 'HH:MI:SS - DD.MM.YYYY')); --������ � ������ ����������
		
		ELSIF (BC = BC2) THEN
			--���� ������ ����������� (�� ����) ����� ������ �����������(����) ---��� �������������� �����������---
		
			UPDATE TEST_BANK_ACC
				 SET TEST_BANK_ACC.DEBIT = TEST_BANK_ACC.DEBIT + VAL
			 WHERE TEST_BANK_ACC.ID = IDACC; --������ ������� �� ����� �����������                          
		
			UPDATE TEST_BANK_ACC
				 SET TEST_BANK_ACC.CREDIT = TEST_BANK_ACC.CREDIT + VAL
			 WHERE TEST_BANK_ACC.ID = IDACC2; --���������� ������� �� ���� ������� ���������� 
		
			INSERT INTO TEST_BANK_TRANSACT
				(ID, BANKACCOUNT_A, BANKACCOUNT_B, AMOUNT, CURR, CMNT, DATE_TRANSC)
			VALUES
				(TEST_IDTRANSACT.NEXTVAL,
				 BN,
				 BN2,
				 VAL,
				 BC || '>' || BC2,
				 '������������ ������� �� ����� ' || BN || ' �� ���� ' || BN2 ||
				 ' ��� ����������� ������ � ' || BA || ' � ' || BA2 ||
				 ', �� ����� ' || BO || ' � ' || BO2,
				 TO_CHAR(SYSTIMESTAMP, 'HH:MI:SS - DD.MM.YYYY')); --������ � ������ ����������                                                 
		END IF;
	
	ELSIF (IDACC2 = 0) THEN
		--���� ���������� ���������� ������� �� ��� ����� � ������� ��� ������ = ���� ������ �����������
	
		FOR i IN forAllAccounts LOOP
		
			UPDATE TEST_BANK_ACC
				 SET TEST_BANK_ACC.DEBIT = TEST_BANK_ACC.DEBIT + VAL
			 WHERE TEST_BANK_ACC.ID = IDACC; --������ ������� �� ����� �����������
		
			UPDATE TEST_BANK_ACC
				 SET TEST_BANK_ACC.CREDIT = TEST_BANK_ACC.CREDIT + VAL
			 WHERE ID = I.ID; --���������� ������� �� ���� ������� ���������� 
		
			INSERT INTO TEST_BANK_TRANSACT
				(ID, BANKACCOUNT_A, BANKACCOUNT_B, AMOUNT, CURR, CMNT, DATE_TRANSC)
			VALUES
				(TEST_IDTRANSACT.NEXTVAL,
				 (SELECT BANKACCOUNT FROM TEST_BANK_ACC WHERE ID = IDACC),
				 I.BANKACCOUNT,
				 VAL,
				 (SELECT CURR FROM TEST_BANK_ACC WHERE ID = IDACC) || '>' ||
				 I.CURR,
				 '������������ ������� �� ����� ' ||
				 (SELECT BANKACCOUNT FROM TEST_BANK_ACC WHERE ID = IDACC) ||
				 ' �� ���� ' || I.BANKACCOUNT || ' ��� ����������� ������ � ' ||
				 (SELECT C.NAMECURR
						FROM TEST_BANK_CATCURR C, TEST_BANK_ACC A
					 WHERE C.CURRID = A.CURR
						 AND A.ID = IDACC) || ' � ' ||
				 (SELECT C.NAMECURR
						FROM TEST_BANK_CATCURR C, TEST_BANK_ACC A
					 WHERE C.CURRID = A.CURR
						 AND A.ID = I.ID) || ', �� ����� ' ||
				 (SELECT C.EXHGRATE
						FROM TEST_BANK_CATCURR C, TEST_BANK_ACC A
					 WHERE C.CURRID = A.CURR
						 AND A.ID = IDACC) || ' � ' ||
				 (SELECT C.EXHGRATE
						FROM TEST_BANK_CATCURR C, TEST_BANK_ACC A
					 WHERE C.CURRID = A.CURR
						 AND A.ID = I.ID),
				 TO_CHAR(SYSTIMESTAMP, 'HH:MI:SS - DD.MM.YYYY')); --������ � ������ ����������
		END LOOP;
	
	END IF;

	COMMIT;
END TEST_TRANSFR_VAL;
/
