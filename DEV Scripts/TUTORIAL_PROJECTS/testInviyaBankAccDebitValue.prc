CREATE OR REPLACE PROCEDURE "TEST_BANKACCDEBITVALUE" (VAL IN NUMBER, IDACC IN NUMBER)
is
       /*��������� ������� ����������� ��� ��������� ���� ������,
			 VAL - �� ����� ���������� ����������� ��� ��������� ����
			 IDACC - ����� ID �������, ���� � IDACC ������ 0 - ����������� �� ���� ������
			 */  
			 
       CURSOR insAcc IS SELECT DEBIT, ID, BANKACCOUNT, CURR
              FROM TEST_BANK_ACC;
			
       CURSOR insBankid IS SELECT BANKACCOUNT,CURR
              FROM TEST_BANK_ACC
							WHERE ID=IDACC;                 
       
			 C TEST_BANK_ACC.CURR%TYPE; 
			 O TEST_BANK_ACC.BANKACCOUNT%TYPE; -- ��������������� �������. ��� ����������� ����� ��������. ���� IDACC != 0
			
       K NUMBER :=0;                             -- �������. ��������

BEGIN
  IF (IDACC = 0) THEN                         --���� �� ������ ����� ID �������
  FOR i IN insAcc LOOP
      k := K+1;        
          UPDATE TEST_BANK_ACC
             SET TEST_BANK_ACC.DEBIT = TEST_BANK_ACC.DEBIT + VAL
             WHERE TEST_BANK_ACC.ID = K;
			
			
			INSERT INTO TEST_BANK_TRANSACT (ID,BANKACCOUNT_A,BANKACCOUNT_B,AMOUNT,CURR,CMNT)
	             VALUES(TEST_IDTRANSACT.NEXTVAL, 26201, i.BANKACCOUNT, VAL, I.CURR,'���������� ����� ������� '||i.BANKACCOUNT||' �� ������� 11011');
					
      DBMS_OUTPUT.PUT_LINE('Bank account: '||i.BANKACCOUNT||' is changed to '||VAL);
  END LOOP;
  
  ELSIF (IDACC != 0) THEN                     --���� ������ ����� ID �������
		    UPDATE TEST_BANK_ACC
             SET TEST_BANK_ACC.DEBIT = TEST_BANK_ACC.DEBIT + VAL
						 WHERE TEST_BANK_ACC.ID = IDACC;
						 
			OPEN insBankid;
				FETCH insBankid INTO O,C;	
					 
				INSERT INTO TEST_BANK_TRANSACT (ID,BANKACCOUNT_A,BANKACCOUNT_B,AMOUNT,CURR,CMNT)
	             VALUES(TEST_IDTRANSACT.NEXTVAL, 26201, O, VAL, C,'���������� ����� �� ������� 11011');
             
			DBMS_OUTPUT.PUT_LINE('Bank account: '||O||' is changed to '||VAL);
			
			CLOSE insBankid;
	ELSE                                       --���������� ���������� ������
		  DBMS_OUTPUT.PUT_LINE('Error!');
	END IF;
	
  COMMIT;
END test_BankAccDebitValue;
/
